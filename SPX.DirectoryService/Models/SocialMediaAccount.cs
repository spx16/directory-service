﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SPX.DirectoryService.Models
{
    public class SocialMediaAccount: Entity
    {        
        public SocialMedia SocialMedia { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public virtual Company Company { get; set; }

        [ForeignKey("Company")]
        public long CompanyId { get; set; }
    }

    public enum SocialMedia
    {
        Facebook = 0,
        Twitter = 1,
        Snapchat = 2,
        Tumblr = 3,
        GooglePlus = 4,        
        Instagram = 5,
        Reddit = 6,
        MySpace = 7,
        Xanga = 8, 

    }
}