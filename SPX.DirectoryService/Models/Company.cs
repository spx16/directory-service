﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SPX.DirectoryService.Models
{
    public class Company: Entity
    {                
        public string Name { get; set; }

        //public virtual Address Address { get; set; }
        public virtual ICollection<SocialMediaAccount> SocialAccounts { get; set; }
        public virtual ICollection<ContactPerson> ContactPeople { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }

    }
}