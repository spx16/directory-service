﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SPX.DirectoryService.Models
{
    public class ContactPerson: Entity
    {
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }

        [JsonIgnore]
        public virtual Company Company { get; set; }

        [ForeignKey("Company")]
        public long CompanyId { get; set; }
    }
}