﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SPX.DirectoryService.Models
{
    
    public class Address: Entity
    {
        [StringLength(100)]        
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(35)]
        public string City { get; set; }

        [StringLength(2)]
        public string State { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }       

        public bool HasChanged(Address address)
        {
            return address.Address1 != Address1 ||
                   address.Address2 != Address2 ||
                   address.City != City ||
                   address.State != State ||
                   address.Zip != Zip;
        }       

        public bool IsPrimary { get; set; }

        [JsonIgnore]
        public Company Company { get; set; }
        
        [ForeignKey("Company")]
        public long CompanyId { get; set; }


        public string Block
        {
            get
            {
                var c = Address1;

                if (!string.IsNullOrEmpty(Address2))
                {
                    c = c + "\n" + Address2;
                }

                c = c + "\n" + City + ", " + State + " " + Zip;

                return c;
            }
        }
        
    }
}