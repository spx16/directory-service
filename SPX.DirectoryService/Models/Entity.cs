﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SPX.DirectoryService.Models
{
    public class Entity : ISoftDelete
    {
        //[Key, Column(Order = 0)]
        //[JsonIgnore]
        //public Int64 Id { get; set; }

        [Key]
        public long Id { get; set; }
        
        [Required]
        public Guid ExternalId { get; set; }
       
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
                
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }

        [JsonIgnore]
        public bool IsDeleted { get; set; }


    }
}
