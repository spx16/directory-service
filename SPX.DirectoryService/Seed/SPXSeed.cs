﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPX.DirectoryService.DAL;
using SPX.DirectoryService.Models;

namespace SPX.DirectoryService.Seed
{
    public class SPXSeed: DropCreateDatabaseAlways<SPXContext>
    {
        protected override void Seed(SPXContext context)
        {

            var companies = new List<Company>
            {
                new Company {Name = "Pizza Palace", ExternalId = Guid.Parse("FE621988-D277-49D1-8BA1-977305DB2939"), CreatedAt = DateTime.Now, Id = 1},
                new Company {Name = "Meetup", ExternalId = Guid.Parse("B082DAAA-0ACE-4D1D-B121-0078835AFFC4"), CreatedAt = DateTime.Now, Id = 2 },
                new Company {Name = "Sharecare", ExternalId = Guid.Parse("DFA8577A-C282-4B26-AFA8-25AE6734A807"), CreatedAt = DateTime.Now, Id = 3 },
                new Company {Name = "IgnitionOne", ExternalId = Guid.Parse("22F52FF1-AC00-44B1-9D1A-C5D6B50282DE"), CreatedAt = DateTime.Now, Id = 4 },
                new Company {Name = "Joe's Auto Shop", ExternalId = Guid.Parse("F47DC6EA-29F7-42AC-AF38-38419579522E"), CreatedAt = DateTime.Now, Id = 5},
                new Company {Name = "Wael's World of Washers and Dryers", ExternalId = Guid.Parse("7A597364-F38D-43B9-94E7-FF1B824066BD"), CreatedAt = DateTime.Now, Id = 6},
                new Company {Name = "Clint's Countertops", ExternalId = Guid.Parse("DE26E6C0-34F9-46DF-BC88-598CD672C652"), CreatedAt = DateTime.Now, Id = 7},
                new Company {Name = "Meetup", ExternalId = Guid.Parse("6D19FFA2-7C6B-471D-9B4E-2C00A7424B59"), CreatedAt = DateTime.Now, Id = 8 },
            };

            companies.ForEach(x => context.Companies.Add(x));

            var socialMedia = new List<SocialMediaAccount>
            {
                new SocialMediaAccount { SocialMedia = SocialMedia.Facebook, Description = "Like Pizza Palace Now on Facebook", CompanyId = 1, ExternalId = Guid.Parse("C6B3A6A1-521E-4AA5-A6D1-6867E6ED1FFE"), CreatedAt = DateTime.Now },
                new SocialMediaAccount { SocialMedia = SocialMedia.GooglePlus, Description = "Like Pizza Palace Now on Google+", CompanyId = 1, ExternalId = Guid.Parse("0A3322A1-54CE-43A3-9666-50DF5BD53AB9"), CreatedAt = DateTime.Now},
                new SocialMediaAccount { SocialMedia = SocialMedia.MySpace, Description = "Like Pizza Palace Now with Tom on MySpace", CompanyId = 1, ExternalId = Guid.Parse("A30A0152-6E96-438F-BC0D-0E437BD41BC2"), CreatedAt = DateTime.Now },
            };

            socialMedia.ForEach(x => context.SocialAccounts.Add(x));

            var addresses = new List<Address>
            {
                new Address { Address1 = "123 Pizza Place", Address2 = "Suite 2000", City = "New York", State = "New York", Zip="70345", CompanyId = 1, CreatedAt =DateTime.Now, IsPrimary = true, ExternalId = Guid.Parse("E8384A77-B2EA-49F7-AB21-E5574817143F") },
                new Address { Address1 = "123 Peachtree Place", Address2 = "Suite 100", City = "Atlanta", State = "Georgia", Zip="30345", CompanyId = 1, CreatedAt =DateTime.Now, IsPrimary = false, ExternalId = Guid.Parse("260E45F6-3B82-4909-A98B-F421187DC4B2") }
            };

            addresses.ForEach(x => context.Addresses.Add(x));            
            
            
            base.Seed(context);
        }
    }
}
