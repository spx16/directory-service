﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using MySql.Data.Entity;
using SPX.DirectoryService.DAL;

namespace SPX.DirectoryService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            DbConfiguration.SetConfiguration(new MySqlEFConfiguration());            
            SPXContext db = new SPXContext();
        }
    }
}
