﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SPX.DirectoryService.DAL;
using SPX.DirectoryService.Models;
using EntityState = System.Data.Entity.EntityState;

namespace SPX.DirectoryService.Controllers
{
    public class AddressController : ApiController
    {
        private readonly SPXContext db = new SPXContext();
        // GET: api/Address
        //public IQueryable<Address> GetAddresses()
        //{
        //    return db.Addresses;
        //}

        // GET: api/Address/guid
        [ResponseType(typeof (Address))]
        public async Task<IHttpActionResult> GetAddress(Guid id)
        {
            var address = await db.Addresses.SingleOrDefaultAsync(x=> x.ExternalId ==id);
            if (address == null)
            {
                return NotFound();
            }

            return Ok(address);
        }

        // PUT: api/Address/5
        [ResponseType(typeof (void))]
        public async Task<IHttpActionResult> PutAddress(Guid id, Address address)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != address.ExternalId)
            {
                return BadRequest();
            }

            Address oldAddress = await db.Addresses.SingleOrDefaultAsync(x => x.ExternalId == id);

            if (oldAddress?.CompanyId != null)
            {
                Company company = await db.Companies.SingleOrDefaultAsync(x => x.Id == oldAddress.CompanyId);

                if (company == null)
                {
                    return BadRequest();
                }

            }
            
            db.Entry(address).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AddressExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Address/
        [HttpPost]
        [Route("api/Address/{companyId}")]
        [ResponseType(typeof (Address))]
        public async Task<IHttpActionResult> PostAddress(Address address, Guid companyId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Company company = await db.Companies.FirstOrDefaultAsync(x => x.ExternalId == companyId);

            if (company == null)
            {
                return BadRequest();
            }

            db.Addresses.Add(address);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AddressExists(address.ExternalId))
                {
                    return Conflict();
                }
                throw;
            }

            return CreatedAtRoute("DefaultApi", new {id = address.ExternalId}, address);
        }

        // DELETE: api/Address/5
        [ResponseType(typeof (Address))]
        public async Task<IHttpActionResult> DeleteAddress(Guid id)
        {
            var address = await db.Addresses.FindAsync(id);
            if (address == null)
            {
                return NotFound();
            }

            address.IsDeleted = true;
            db.Entry(address).State = System.Data.Entity.EntityState.Modified;

            await db.SaveChangesAsync();

            return Ok(address);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AddressExists(Guid id)
        {
            return db.Addresses.Count(e => e.ExternalId == id) > 0;
        }
    }
}