﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SPX.DirectoryService.DAL;
using SPX.DirectoryService.Models;

using EntityState = System.Data.Entity.EntityState;
namespace SPX.DirectoryService.Controllers
{
    [RoutePrefix("Company")]
    public class CompanyController : ApiController
    {
        private SPXContext db = new SPXContext();

        // GET: api/Company
        public IQueryable<Company> GetCompanies()
        {
            return db.Companies;
        }

        // GET: api/Company/[Guid]
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> GetCompany(Guid externalId)
        {
            Company company = await db.Companies.FirstOrDefaultAsync(x => x.ExternalId == externalId);
            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }

        // PUT: api/Company/[Guid]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCompany(Guid id, Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != company.ExternalId)
            {
                return BadRequest();
            }

            db.Entry(company).State = System.Data.Entity.EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Company
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> PostCompany(Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Companies.Add(company);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CompanyExists(company.ExternalId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = company.Id }, company);
        }

        // DELETE: api/Company/5
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> DeleteCompany(Guid externalId)
        {
            Company company = await db.Companies.FirstOrDefaultAsync(x => x.ExternalId == externalId);
            if (company == null)
            {
                return NotFound();
            }

            company.IsDeleted = true;
            db.Entry(company).State = System.Data.Entity.EntityState.Modified;

            await db.SaveChangesAsync();

            return Ok(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyExists(Guid externalId)
        {
            return db.Companies.Count(e => e.ExternalId == externalId) > 0;
        }
    }
}