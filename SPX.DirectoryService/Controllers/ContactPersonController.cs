﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SPX.DirectoryService.Models;
using SPX.DirectoryService.DAL;
using EntityState = System.Data.Entity.EntityState;

namespace SPX.DirectoryService.Controllers
{
    public class ContactPersonController : ApiController
    {
        private SPXContext db = new SPXContext();

        //// GET: api/ContactPerson
        //public IQueryable<ContactPerson> GetContactPeople()
        //{
        //    return db.ContactPeople;
        //}

        // GET: api/ContactPerson/5
        [ResponseType(typeof(ContactPerson))]
        public async Task<IHttpActionResult> GetContactPerson(Guid id)
        {
            ContactPerson contactPerson = await db.ContactPeople.FirstOrDefaultAsync(x=> x.ExternalId ==  id);
            if (contactPerson == null)
            {
                return NotFound();
            }

            return Ok(contactPerson);
        }

        // PUT: api/ContactPerson/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContactPerson(Guid id, ContactPerson contactPerson)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contactPerson.ExternalId)
            {
                return BadRequest();
            }

            ContactPerson oldContactPerson = await db.ContactPeople.SingleOrDefaultAsync(x => x.ExternalId == id);

            if (oldContactPerson?.CompanyId != null)
            {
                Company company = await db.Companies.SingleOrDefaultAsync(x => x.Id == oldContactPerson.CompanyId);

                if (company == null)
                {
                    return BadRequest();
                }

            }

            db.Entry(contactPerson).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactPersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ContactPerson
        [HttpPost]
        [Route("api/ContactPerson/{companyId}")]
        [ResponseType(typeof(ContactPerson))]
        public async Task<IHttpActionResult> PostContactPerson(ContactPerson contactPerson, Guid companyId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Company company = await db.Companies.FirstOrDefaultAsync(x => x.ExternalId == companyId);

            if (company == null)
            {
                return BadRequest();
            }

            db.ContactPeople.Add(contactPerson);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = contactPerson.Id }, contactPerson);
        }

        // DELETE: api/ContactPerson/5
        [ResponseType(typeof(ContactPerson))]
        public async Task<IHttpActionResult> DeleteContactPerson(Guid id)
        {
            ContactPerson contactPerson = await db.ContactPeople.FirstOrDefaultAsync(x=> x.ExternalId == id);
            if (contactPerson == null)
            {
                return NotFound();
            }

            contactPerson.IsDeleted = true;
            db.Entry(contactPerson).State = System.Data.Entity.EntityState.Modified;
            
            await db.SaveChangesAsync();

            return Ok(contactPerson);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactPersonExists(Guid id)
        {
            return db.ContactPeople.Count(e => e.ExternalId == id) > 0;
        }
    }
}