﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SPX.DirectoryService.Models;
using SPX.DirectoryService.DAL;
using EntityState = System.Data.Entity.EntityState;

namespace SPX.DirectoryService.Controllers
{
    public class SocialMediaAccountController : ApiController
    {
        private SPXContext db = new SPXContext();

        // GET: api/SocialMediaAccount
        //public IQueryable<SocialMediaAccount> GetSocialAccounts()
        //{
        //    return db.SocialAccounts;
        //}

        // GET: api/SocialMediaAccount/5
        [ResponseType(typeof(SocialMediaAccount))]
        public async Task<IHttpActionResult> GetSocialMediaAccount(Guid id)
        {
            SocialMediaAccount socialMediaAccount = await db.SocialAccounts.FirstOrDefaultAsync(x=> x.ExternalId == id);
            if (socialMediaAccount == null)
            {
                return NotFound();
            }

            return Ok(socialMediaAccount);
        }

        // PUT: api/SocialMediaAccount/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSocialMediaAccount(Guid id, SocialMediaAccount socialMediaAccount)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != socialMediaAccount.ExternalId)
            {
                return BadRequest();
            }

            SocialMediaAccount oldSocialMediaAcount = await db.SocialAccounts.SingleOrDefaultAsync(x => x.ExternalId == id);

            if (oldSocialMediaAcount?.CompanyId != null)
            {
                Company company = await db.Companies.SingleOrDefaultAsync(x => x.Id == oldSocialMediaAcount.CompanyId);

                if (company == null)
                {
                    return BadRequest();
                }

            }


            db.Entry(socialMediaAccount).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SocialMediaAccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [HttpPost]
        [Route("api/SocialMediaAccount/{companyId}")]
        // POST: api/SocialMediaAccount
        [ResponseType(typeof(SocialMediaAccount))]
        public async Task<IHttpActionResult> PostSocialMediaAccount(SocialMediaAccount socialMediaAccount, Guid companyId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Company company = await db.Companies.FirstOrDefaultAsync(x => x.ExternalId == companyId);

            if (company == null)
            {
                return BadRequest();
            }

            db.SocialAccounts.Add(socialMediaAccount);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = socialMediaAccount.Id }, socialMediaAccount);
        }

        // DELETE: api/SocialMediaAccount/5
        [ResponseType(typeof(SocialMediaAccount))]
        public async Task<IHttpActionResult> DeleteSocialMediaAccount(long id)
        {
            SocialMediaAccount socialMediaAccount = await db.SocialAccounts.FindAsync(id);
            if (socialMediaAccount == null)
            {
                return NotFound();
            }

            socialMediaAccount.IsDeleted = true;
            db.Entry(socialMediaAccount).State = System.Data.Entity.EntityState.Modified;

            await db.SaveChangesAsync();

            return Ok(socialMediaAccount);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SocialMediaAccountExists(Guid id)
        {
            return db.SocialAccounts.Count(e => e.ExternalId == id) > 0;
        }
    }
}