namespace SPX.DirectoryService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                    c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Address1 = c.String(maxLength: 100, storeType: "nvarchar"),
                        Address2 = c.String(maxLength: 100, storeType: "nvarchar"),
                        City = c.String(maxLength: 35, storeType: "nvarchar"),
                        State = c.String(maxLength: 2, storeType: "nvarchar"),
                        Zip = c.String(maxLength: 10, storeType: "nvarchar"),
                        IsPrimary = c.Boolean(nullable: false),
                        CompanyId = c.Long(nullable: false),
                        externalId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(nullable: false, precision: 0),
                        ModifiedAt = c.DateTime(nullable: false, precision: 0),
                        CreatedBy = c.Guid(nullable: false),
                        ModifiedBy = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Company", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        externalId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(nullable: false, precision: 0),
                        ModifiedAt = c.DateTime(nullable: false, precision: 0),
                        CreatedBy = c.Guid(nullable: false),
                        ModifiedBy = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContactPerson",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Salutation = c.String(unicode: false),
                        FirstName = c.String(unicode: false),
                        LastName = c.String(unicode: false),
                        Title = c.String(unicode: false),
                        Phone = c.String(unicode: false),
                        CompanyId = c.Long(nullable: false),
                        externalId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(nullable: false, precision: 0),
                        ModifiedAt = c.DateTime(nullable: false, precision: 0),
                        CreatedBy = c.Guid(nullable: false),
                        ModifiedBy = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Company", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.SocialMediaAccount",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SocialMedia = c.Int(nullable: false),
                        UserName = c.String(unicode: false),
                        Description = c.String(unicode: false),
                        CompanyId = c.Long(nullable: false),
                        externalId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(nullable: false, precision: 0),
                        ModifiedAt = c.DateTime(nullable: false, precision: 0),
                        CreatedBy = c.Guid(nullable: false),
                        ModifiedBy = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Company", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Address", "CompanyId", "dbo.Company");
            DropForeignKey("dbo.SocialMediaAccount", "CompanyId", "dbo.Company");
            DropForeignKey("dbo.ContactPerson", "CompanyId", "dbo.Company");
            DropIndex("dbo.SocialMediaAccount", new[] { "CompanyId" });
            DropIndex("dbo.ContactPerson", new[] { "CompanyId" });
            DropIndex("dbo.Address", new[] { "CompanyId" });
            DropTable("dbo.SocialMediaAccount");
            DropTable("dbo.ContactPerson");
            DropTable("dbo.Company");
            DropTable("dbo.Address");
        }
    }
}
