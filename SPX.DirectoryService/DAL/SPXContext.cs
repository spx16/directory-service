﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.Entity;
using SPX.DirectoryService.Models;
using SPX.DirectoryService.Seed;
using EntityFramework.DynamicFilters;

namespace SPX.DirectoryService.DAL
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class SPXContext : DbContext
    {
        public SPXContext() : base("SPXContext")
        {
            Database.SetInitializer(new SPXSeed());
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ContactPerson> ContactPeople { get; set; }
        public DbSet<SocialMediaAccount> SocialAccounts { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Filter("IsDeleted", (ISoftDelete d) => d.IsDeleted, false);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
    
    
}
